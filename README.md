vagrantfile
===========

Vagrantfile examples for different environments using libvirt+kvm, openvswitch, etc.


License
-------

MIT

Author Information
------------------

Fabricio Boreli - fabricioboreli26 at gmail dot com
